From 9121b84dac6ef0593bf19464da448cd230e70d33 Mon Sep 17 00:00:00 2001
From: ut003409 <caopeiqing@uniontech.com>
Date: Thu, 7 Nov 2024 09:35:23 +0800
Subject: [PATCH] feat: add GMTLS support.

---
 docs/curl.1                      | 21 ++++++++++++++++++++-
 docs/libcurl/symbols-in-versions |  2 ++
 include/curl/curl.h              |  6 ++++++
 lib/vtls/mbedtls.c               |  3 +++
 lib/vtls/openssl.c               | 22 ++++++++++++++++++++++
 lib/vtls/schannel.c              |  3 +++
 src/tool_getparam.c              |  9 +++++++++
 src/tool_listhelp.c              |  4 ++++
 src/tool_setopt.c                |  1 +
 9 files changed, 70 insertions(+), 1 deletion(-)

diff --git a/docs/curl.1 b/docs/curl.1
index 6b192a7..d2cf159 100644
--- a/docs/curl.1
+++ b/docs/curl.1
@@ -4950,7 +4950,7 @@ Examples:
 See also \fI\-\-etag\-compare\fP and \fI-R, \-\-remote\-time\fP.
 .IP "\-\-tls\-max <VERSION>"
 (TLS) VERSION defines maximum supported TLS version. The minimum acceptable version
-is set by tlsv1.0, tlsv1.1, tlsv1.2 or tlsv1.3.
+is set by tlsv1.0, tlsv1.1, tlsv1.2, tlsv1.3 or GMTLSv1.1.
 
 If the connection is done without TLS, this option has no effect. This
 includes QUIC\-using (HTTP/3) transfers.
@@ -4966,6 +4966,8 @@ Use up to TLSv1.1.
 Use up to TLSv1.2.
 .IP "1.3"
 Use up to TLSv1.3.
+.IP "1.1"
+Use up to GMTLSv1.1.
 .RE
 .IP
 
@@ -5101,6 +5103,23 @@ Example:
  curl \-\-tlsv1.3 https://example.com
 .fi
 
+See also \fI\-\-tlsv1.3\fP and \fI\-\-tls\-max\fP.
+.IP "\-\-gmtlsv1.1"
+(TLS) Forces curl to use gmtlsv1.1 or later when connecting to a remote TLS
+server.
+
+If the connection is done without gmtlsv1, this option has no effect. This
+includes QUIC\-using (HTTP/3) transfers.
+
+Note that gmtlsv1.1 is not supported by all gmtlsv1 backends.
+
+Providing \fI\-\-gmtlsv1\fP multiple times has no extra effect.
+
+Example:
+.nf
+ curl \-\-gmtlsv1 https://example.com
+.fi
+
 See also \fI\-\-tlsv1.2\fP and \fI\-\-tls\-max\fP. Added in 7.52.0.
 .IP "\-1, \-\-tlsv1"
 (TLS) Tells curl to use at least TLS version 1.x when negotiating with a remote TLS
diff --git a/docs/libcurl/symbols-in-versions b/docs/libcurl/symbols-in-versions
index 24a954e..8758073 100644
--- a/docs/libcurl/symbols-in-versions
+++ b/docs/libcurl/symbols-in-versions
@@ -138,6 +138,7 @@ CURL_SSLVERSION_MAX_TLSv1_0     7.54.0
 CURL_SSLVERSION_MAX_TLSv1_1     7.54.0
 CURL_SSLVERSION_MAX_TLSv1_2     7.54.0
 CURL_SSLVERSION_MAX_TLSv1_3     7.54.0
+CURL_SSLVERSION_MAX_GMTLSv1_1   7.71.1
 CURL_SSLVERSION_SSLv2           7.9.2
 CURL_SSLVERSION_SSLv3           7.9.2
 CURL_SSLVERSION_TLSv1           7.9.2
@@ -145,6 +146,7 @@ CURL_SSLVERSION_TLSv1_0         7.34.0
 CURL_SSLVERSION_TLSv1_1         7.34.0
 CURL_SSLVERSION_TLSv1_2         7.34.0
 CURL_SSLVERSION_TLSv1_3         7.52.0
+CURL_SSLVERSION_GMTLSv1_1       7.71.1
 CURL_STRICTER                   7.50.2
 CURL_TIMECOND_IFMODSINCE        7.9.7
 CURL_TIMECOND_IFUNMODSINCE      7.9.7
diff --git a/include/curl/curl.h b/include/curl/curl.h
index bf71d82..6621948 100644
--- a/include/curl/curl.h
+++ b/include/curl/curl.h
@@ -2324,6 +2324,9 @@ enum {
   CURL_SSLVERSION_TLSv1_1,
   CURL_SSLVERSION_TLSv1_2,
   CURL_SSLVERSION_TLSv1_3,
+#ifndef OPENSSL_NO_SM2
+  CURL_SSLVERSION_GMTLSv1_1,
+#endif // OPENSSL_NO_SM2
 
   CURL_SSLVERSION_LAST /* never use, keep last */
 };
@@ -2335,6 +2338,9 @@ enum {
   CURL_SSLVERSION_MAX_TLSv1_1 =  (CURL_SSLVERSION_TLSv1_1 << 16),
   CURL_SSLVERSION_MAX_TLSv1_2 =  (CURL_SSLVERSION_TLSv1_2 << 16),
   CURL_SSLVERSION_MAX_TLSv1_3 =  (CURL_SSLVERSION_TLSv1_3 << 16),
+#ifndef OPENSSL_NO_SM2
+  CURL_SSLVERSION_MAX_GMTLSv1_1 =  (CURL_SSLVERSION_GMTLSv1_1 << 16),
+#endif // OPENSSL_NO_SM2
 
   /* never use, keep last */
   CURL_SSLVERSION_MAX_LAST =     (CURL_SSLVERSION_LAST    << 16)
diff --git a/lib/vtls/mbedtls.c b/lib/vtls/mbedtls.c
index 2f994d7..1d00c25 100644
--- a/lib/vtls/mbedtls.c
+++ b/lib/vtls/mbedtls.c
@@ -577,6 +577,9 @@ mbed_connect_step1(struct Curl_cfilter *cf, struct Curl_easy *data)
   case CURL_SSLVERSION_TLSv1_1:
   case CURL_SSLVERSION_TLSv1_2:
   case CURL_SSLVERSION_TLSv1_3:
+#ifndef OPENSSL_NO_SM2
+  case CURL_SSLVERSION_GMTLSv1_1:
+#endif // OPENSSL_NO_SM2
     {
       CURLcode result = set_ssl_version_min_max(cf, data);
       if(result != CURLE_OK)
diff --git a/lib/vtls/openssl.c b/lib/vtls/openssl.c
index 6be86f8..fd28334 100644
--- a/lib/vtls/openssl.c
+++ b/lib/vtls/openssl.c
@@ -2662,6 +2662,11 @@ static void ossl_trace(int direction, int ssl_ver, int content_type,
   case TLS1_3_VERSION:
     verstr = "TLSv1.3";
     break;
+#endif
+#ifdef GMTLS1_1_VERSION
+  case GMTLS1_1_VERSION:
+    verstr = "GMTLSv1.1";
+    break;
 #endif
   case 0:
     break;
@@ -3496,6 +3501,12 @@ static CURLcode ossl_connect_step1(struct Curl_cfilter *cf,
 #endif
     use_sni(TRUE);
     break;
+#ifndef OPENSSL_NO_SM2
+  case CURL_SSLVERSION_GMTLSv1_1:
+    req_method = GMTLSv1_1_client_method();
+    use_sni(TRUE);
+    break;
+#endif // OPENSSL_NO_SM2
   case CURL_SSLVERSION_SSLv2:
     failf(data, "No SSLv2 support");
     return CURLE_NOT_BUILT_IN;
@@ -3603,6 +3614,9 @@ static CURLcode ossl_connect_step1(struct Curl_cfilter *cf,
   case CURL_SSLVERSION_TLSv1_1: /* TLS >= version 1.1 */
   case CURL_SSLVERSION_TLSv1_2: /* TLS >= version 1.2 */
   case CURL_SSLVERSION_TLSv1_3: /* TLS >= version 1.3 */
+#ifndef OPENSSL_NO_SM2
+  case CURL_SSLVERSION_GMTLSv1_1:
+#endif // OPENSSL_NO_SM2
     /* asking for any TLS version as the minimum, means no SSL versions
        allowed */
     ctx_options |= SSL_OP_NO_SSLv2;
@@ -4258,6 +4272,14 @@ static CURLcode servercert(struct Curl_cfilter *cf,
       infof(data, " SSL certificate verify ok.");
   }
 
+  /* if use gmssl,set gmssl ciphers */
+#ifndef OPENSSL_NO_SM2
+  char* version = SSL_get_version(backend->handle);
+	if(version[0] == 'G' && version[1] == 'M'){
+		SSL_set_cipher_list(backend->handle, "ECC-SM4-SM3");
+	}
+#endif // OPENSSL_NO_SM2
+
 #if (OPENSSL_VERSION_NUMBER >= 0x0090808fL) && !defined(OPENSSL_NO_TLSEXT) && \
   !defined(OPENSSL_NO_OCSP)
   if(conn_config->verifystatus) {
diff --git a/lib/vtls/schannel.c b/lib/vtls/schannel.c
index 410a5c4..45aefeb 100644
--- a/lib/vtls/schannel.c
+++ b/lib/vtls/schannel.c
@@ -521,6 +521,9 @@ schannel_acquire_credential_handle(struct Curl_cfilter *cf,
   case CURL_SSLVERSION_TLSv1_1:
   case CURL_SSLVERSION_TLSv1_2:
   case CURL_SSLVERSION_TLSv1_3:
+#ifndef OPENSSL_NO_SM2
+  case CURL_SSLVERSION_GMTLSv1_1:
+#endif // OPENSSL_NO_SM2
   {
     result = schannel_set_ssl_version_min_max(&enabled_protocols, cf, data);
     if(result != CURLE_OK)
diff --git a/src/tool_getparam.c b/src/tool_getparam.c
index d9772a3..c1fd9df 100644
--- a/src/tool_getparam.c
+++ b/src/tool_getparam.c
@@ -224,6 +224,9 @@ static const struct LongShort aliases[]= {
   {"11",  "tlsv1.1",                 ARG_NONE},
   {"12",  "tlsv1.2",                 ARG_NONE},
   {"13",  "tlsv1.3",                 ARG_NONE},
+#ifndef OPENSSL_NO_SM2
+  {"1g", "gmtlsv1.1",                    ARG_NONE},
+#endif // OPENSSL_NO_SM2
   {"1A", "tls13-ciphers",            ARG_STRING},
   {"1B", "proxy-tls13-ciphers",      ARG_STRING},
   {"2",  "sslv2",                    ARG_NONE},
@@ -1625,6 +1628,12 @@ ParameterError getparameter(const char *flag, /* f or -long-flag */
         /* TLS version 1.3 */
         config->ssl_version = CURL_SSLVERSION_TLSv1_3;
         break;
+#ifndef OPENSSL_NO_SM2
+      case 'g':
+        /* GMTLS version 1.1 */
+        config->ssl_version = CURL_SSLVERSION_GMTLSv1_1;
+        break;
+#endif // OPENSSL_NO_SM2
       case 'A': /* --tls13-ciphers */
         GetStr(&config->cipher13_list, nextarg);
         break;
diff --git a/src/tool_listhelp.c b/src/tool_listhelp.c
index 4e7a6dd..0f07c35 100644
--- a/src/tool_listhelp.c
+++ b/src/tool_listhelp.c
@@ -753,6 +753,10 @@ const struct helptxt helptext[] = {
   {"    --tlsv1.3",
    "Use TLSv1.3 or greater",
    CURLHELP_TLS},
+#ifndef OPENSSL_NO_SM2
+  {"    --gmtlsv1.1",
+   "Use GMTLSv1.1 or greater"},
+#endif // OPENSSL_NO_SM2   
   {"    --tr-encoding",
    "Request compressed transfer encoding",
    CURLHELP_HTTP},
diff --git a/src/tool_setopt.c b/src/tool_setopt.c
index de3b78f..868fbf9 100644
--- a/src/tool_setopt.c
+++ b/src/tool_setopt.c
@@ -104,6 +104,7 @@ const struct NameValue setopt_nv_CURL_SSLVERSION[] = {
   NV(CURL_SSLVERSION_TLSv1_1),
   NV(CURL_SSLVERSION_TLSv1_2),
   NV(CURL_SSLVERSION_TLSv1_3),
+  NV(CURL_SSLVERSION_GMTLSv1_1),
   NVEND,
 };
 
-- 
2.20.1

